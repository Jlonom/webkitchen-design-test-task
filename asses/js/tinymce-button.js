(function() {
    tinymce.create('tinymce.plugins.action_filter', {
        init : function(ed, url) {
            ed.addButton('action_filter', {
                title : 'Actions filter',
                image : url+'/../images/AF.png',
                onclick : function() {
                    ed.selection.setContent('[actions_filter]');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('action_filter', tinymce.plugins.action_filter);
})();
