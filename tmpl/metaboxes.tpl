<?php
/**
 * @var $date_from string
 * @var $date_to string
 */
?>
<input type="hidden" name="action_nonce" value="<?php echo wp_create_nonce("WD_TEST_TASK"); ?>"/>
<table class="form-table">
    <tr>
        <th><label for="date_from"><?php echo __("Date from", "wd-test-task"); ?></label></th>
        <td><input type="text" id="date_from" name="date_from" value="<?php echo $date_from; ?>" required /></td>
    </tr>
    <tr>
        <th><label for="date_to"><?php echo __("Date to", "wd-test-task"); ?></label></th>
        <td><input type="text" id="date_to" name="date_to" value="<?php echo $date_to; ?>" required /></td>
    </tr>
</table>

