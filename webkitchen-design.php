<?php
/**
    Plugin Name: Webkitchen Design Test Task
    Plugin URI: https://bitbucket.org/Jlonom/webkitchen-design-test-task/
    Description: Test task for Webkitchen Design Company
    Author: Aleksandr Martynenko
    Version: 1.0
    Author URI: http://freelance.ua/user/Jlonom/portfolio/
    Text Domain: wd-test-task
*/
/**
 * @package wd-test-task
 */
define("WD_DS", DIRECTORY_SEPARATOR);
define("WD_PLUGIN_DIR", realpath(__DIR__) . WD_DS);
define("WD_PLUGIN_CORE", WD_PLUGIN_DIR . WD_DS . "core" . WD_DS);
define("WD_PLUGIN_URL", plugin_dir_url( __FILE__ ) . WD_DS);

include WD_PLUGIN_CORE . "WD_Core.php";
include WD_PLUGIN_CORE . "WD_PostType.php";
include WD_PLUGIN_CORE . "WD_Styles.php";
include WD_PLUGIN_CORE . "WD_TinyMCE_Button.php";

WD_Core::init();