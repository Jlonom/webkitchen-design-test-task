<?php

/**
 * Class WD_Core
 * @package wd-test-task
 */
class WD_Core
{
    const WD_TEMPLATES_DIR = WD_PLUGIN_DIR . "tmpl/";

    public static function init()
    {
        WD_PostType::init();
        WD_Styles::init();
        WD_TinyMCE_Button::init();
    }

    protected function render($template_name, $data)
    {
        $filename = strtolower($template_name) . '.tpl';
        $file = self::WD_TEMPLATES_DIR . $filename;
        if (!file_exists($file)) {
            return realpath($file);
        }

        extract($data);
        ob_start();
        include ($file);
        $content = ob_get_contents();
        ob_clean();

        return $content;
    }
}