<?php
/**
 * Class WD_PostType
 * @package wd-test-task
 */
class WD_PostType extends WD_Core
{
    const WD_TEMPLATES_DIR = WD_PLUGIN_DIR . "tmpl/";

    public static function init()
    {
        $plugin = new WD_PostType;
        add_action("init", array($plugin, "createPostType"));
        add_action("init", array($plugin, "createTaxonomies"));
        add_action("add_meta_boxes", array($plugin, "addMetaBox"), 10, 2 );
        add_action('save_post', array($plugin, "saveMetaData"));
    }

    /**
     * Adding "Action" post type
     */
    public function createPostType()
    {
        $labels = array(
            "name" => __("Action", "wd-test-task"),
            "singular_name" => __("Action", "wd-test-task"),
            "add_new" => __("Add new action", "wd-test-task"),
            "add_new_item" => __("Add new action", "wd-test-task"),
            "edit_item" => __("Edit action", "wd-test-task"),
            "new_item" => __("New action", "wd-test-task"),
            "view_item" => __("View action", "wd-test-task"),
            "search_items" => __("Search actions", "wd-test-task"),
            "not_found" =>  __("Action not found", "wd-test-task"),
            "not_found_in_trash" => __("Action not found in trash", "wd-test-task"),
            "menu_name" => __("Actions", "wd-test-task")
        );
        register_post_type("action", array(
            "labels" => $labels,
            "public" => true,
            "publicly_queryable" => false,
            "exclude_from_search" => true,
            "show_ui" => true,
            "show_in_menu" => true,
            "menu_icon" =>  "dashicons-location",
            "query_var" => true,
            "rewrite" => true,
            "capability_type" => "post",
            "has_archive" => true,
            "hierarchical" => false,
            "menu_position" => null,
            "supports" => array("title", "editor")
        ));
    }

    /**
     * Adding taxonomies for "Action" post type
     */
    public function createTaxonomies()
    {
        register_taxonomy( "action_city", array("action"), array(
            'labels'                => array(
                'name'              => __('Cities', "wd-test-task"),
                'singular_name'     => __('City', "wd-test-task"),
                'search_items'      => __('Search cities', "wd-test-task"),
                'all_items'         => __('All cities', "wd-test-task"),
                'edit_item'         => __('Edit city', "wd-test-task"),
                'update_item'       => __('Update city', "wd-test-task"),
                'add_new_item'      => __('Add new city', "wd-test-task"),
                'new_item_name'     => __('New city name', "wd-test-task"),
                'menu_name'         => __('Cities', "wd-test-task"),
            ),
            'public'                => true,
            'publicly_queryable'    => null,
            'hierarchical'          => true,
            'rewrite'               => true,
            'show_admin_column'     => false,
            '_builtin'              => false,
            'show_in_quick_edit'    => null,
        ) );
        register_taxonomy( "action_type", array("action"), array(
            'labels'                => array(
                'name'              => __('Types', "wd-test-task"),
                'singular_name'     => __('Type', "wd-test-task"),
                'search_items'      => __('Search types', "wd-test-task"),
                'all_items'         => __('All types', "wd-test-task"),
                'edit_item'         => __('Edit type', "wd-test-task"),
                'update_item'       => __('Update type', "wd-test-task"),
                'add_new_item'      => __('Add new type', "wd-test-task"),
                'new_item_name'     => __('New type name', "wd-test-task"),
                'menu_name'         => __('Types', "wd-test-task"),
            ),
            'public'                => true,
            'publicly_queryable'    => null,
            'hierarchical'          => true,
            'rewrite'               => true,
            'show_admin_column'     => false,
            '_builtin'              => false,
            'show_in_quick_edit'    => null,
        ) );
        register_taxonomy( "action_category", array("action"), array(
            'labels'                => array(
                'name'              => __('Categories', "wd-test-task"),
                'singular_name'     => __('Category', "wd-test-task"),
                'search_items'      => __('Search categories', "wd-test-task"),
                'all_items'         => __('All categories', "wd-test-task"),
                'edit_item'         => __('Edit category', "wd-test-task"),
                'update_item'       => __('Update category', "wd-test-task"),
                'add_new_item'      => __('Add new category', "wd-test-task"),
                'new_item_name'     => __('New category name', "wd-test-task"),
                'menu_name'         => __('Categories', "wd-test-task"),
            ),
            'public'                => true,
            'publicly_queryable'    => null,
            'hierarchical'          => true,
            'rewrite'               => true,
            'show_admin_column'     => false,
            '_builtin'              => false,
            'show_in_quick_edit'    => null,
        ) );
    }

    /**
     * Adding MetaBox for "Action" post type
     */
    public function addMetaBox()
    {
        add_meta_box(
            'action_metabox',
            __('Action dates', "wd-test-task"),
            function(){
                global $post;
                $date_from = get_post_meta($post->ID, "date_from", true);
                $date_to = get_post_meta($post->ID, "date_to", true);
                echo $this->render("metaboxes", array("date_from" => $date_from, "date_to" => $date_to));
            },
            'action',
            'normal',
            'default'
        );
    }

    /**
     * Save MetaData for "Action" post type
     */
    public function saveMetaData( $post_id )
    {
        if (!wp_verify_nonce($_POST['action_nonce'], "WD_TEST_TASK"))
            return $post_id;

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        $old_date_from = get_post_meta($post_id, "date_from", true);
        $old_date_to = get_post_meta($post_id, "date_to", true);
        $new_date_from = $_POST["date_from"];
        $new_date_to = $_POST["date_to"];
        if ($new_date_from && $new_date_from != $old_date_from) {
            update_post_meta($post_id, "date_from", $new_date_from);
        } elseif ('' == $new_date_from && $old_date_from) {
            delete_post_meta($post_id, "date_from", $old_date_from);
        }
        if ($new_date_to && $new_date_to != $old_date_to) {
            update_post_meta($post_id, "date_to", $new_date_to);
        } elseif ('' == $new_date_to && $old_date_to) {
            delete_post_meta($post_id, "date_to", $old_date_to);
        }
    }
}