<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 02.09.16
 * Time: 15:52
 */
class WD_TinyMCE_Button
{
    public static function init()
    {
        $button = new WD_TinyMCE_Button;
        add_action('init', array($button, "addFilters"));
    }

    public function addFilters()
    {
        add_filter('mce_external_plugins', array($this, "plugin"));
        add_filter('mce_buttons_3', array($this, "registerButton"));
    }

    public function plugin($plugin_array)
    {
        $plugin_array['action_filter'] = WD_PLUGIN_URL . "asses/js/tinymce-button.js";
        return $plugin_array;
    }

    public function registerButton($buttons)
    {
        array_push($buttons, "action_filter");
        return $buttons;
    }
}