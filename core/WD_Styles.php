<?php

/**
 * Class WD_Styles
 * @package wd-test-task
 */
class WD_Styles
{

    public static function init()
    {
        $styles = new WD_Styles;
        add_action('admin_enqueue_scripts', array($styles, 'addAdminStyles'));
        add_action('admin_enqueue_scripts', array($styles, 'addAdminScripts'));
        add_action('wp_enqueue_scripts', array($styles, 'addStyles'));
        add_action('wp_enqueue_scripts', array($styles, 'addScripts'));
    }

    public function addStyles()
    {
        
    }

    public function addScripts()
    {

    }

    public function addAdminStyles()
    {
        wp_enqueue_script( "WD_Jquery-ui", plugins_url( '/asses/js/jquery-ui.min.js', __DIR__ ), array( 'jquery' ) );
        wp_enqueue_script( "WD_Admin", plugins_url( '/asses/js/admin.js', __DIR__ ), array( 'WD_Jquery-ui' ) );
    }

    public function addAdminScripts()
    {
        wp_enqueue_style( "WD_Jquery-ui", plugins_url( '/asses/css/jquery-ui.min.css', __DIR__ ) );
        wp_enqueue_style( "WD_Jquery-ui-theme", plugins_url( '/asses/css/jquery-ui.theme.min.css', __DIR__ ) );
    }
}